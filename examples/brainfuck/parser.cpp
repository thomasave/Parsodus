/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "parser.h"
#include "instruction.h"

#include <iostream>

namespace bf {

Parser::Parser(BfLexer lex) : 
	bfParser<std::unique_ptr<std::deque<std::unique_ptr<Instruction>>>>(), m_lex(lex) {
}

Parser::Token Parser::lex() {
	try {
		
		BfLexer::Token orig = m_lex.nextToken();
        auto pm = std::make_unique<std::deque<std::unique_ptr<Instruction>>>();
		switch(orig.type) {
			case BfLexer::PLUS: {
				pm->push_back(std::make_unique<InstructionInc>());
				//std::cout << "PARSING: found PLUS " << (int)std::unique_ptr<Instruction>::PLUS << std::endl;
				return Token{ bfParser_Symbol::T_PLUS, std::move(pm) };
			} case BfLexer::MINUS: {                  
				pm->push_back(std::make_unique<InstructionDec>());
				//std::cout << "PARSING: found MINUS" << (int)std::unique_ptr<Instruction>::MINUS << std::endl;
                return Token{ bfParser_Symbol::T_MINUS, std::move(pm) };
			} case BfLexer::GREATER: {
				pm->push_back(std::make_unique<InstructionRight>());
				//std::cout << "PARSING: found GREATER" << (int)std::unique_ptr<Instruction>::GREATER << std::endl;
	    		return Token{ bfParser_Symbol::T_GREATER, std::move(pm) };
			} case BfLexer::LESS: {
				pm->push_back(std::make_unique<InstructionLeft>());
				//std::cout << "PARSING: found LESS" << (int)std::unique_ptr<Instruction>::LESS << std::endl;
                return Token{ bfParser_Symbol::T_LESS, std::move(pm) };
			} case BfLexer::POINT: {
				pm->push_back(std::make_unique<InstructionOut>());
				//std::cout << "PARSING: found POINT" << (int)std::unique_ptr<Instruction>::POINT << std::endl;
                return Token{ bfParser_Symbol::T_POINT, std::move(pm) };
			} case BfLexer::COMMA: {
				pm->push_back(std::make_unique<InstructionIn>());
				//std::cout << "PARSING: found COMMA" << (int)std::unique_ptr<Instruction>::COMMA << std::endl;
                return Token{ bfParser_Symbol::T_COMMA, std::move(pm) };
			} case BfLexer::LBRACKET: {
				//std::cout << "PARSING: found LBRACKET" << (int)std::unique_ptr<Instruction>::LBRACKET << std::endl;
                return Token{ bfParser_Symbol::T_LBRACKET, nullptr };
			} case BfLexer::RBRACKET: {
				//std::cout << "PARSING: found RBRACKET" << (int)std::unique_ptr<Instruction>::RBRACKET << std::endl;
                return Token{ bfParser_Symbol::T_RBRACKET, nullptr };
			}
            default:
                return Token { bfParser_Symbol::T_EOF, nullptr };
		}
	} catch(BfLexer::NoMoreTokens) {
		return Token{ bfParser_Symbol::T_EOF, nullptr };
	}
}

std::unique_ptr<std::deque<std::unique_ptr<Instruction>>> Parser::reduce_PROGRAM(std::deque<Token> subparts) {
	// <program> ::= <instruction> <program>
	for(auto& instr : *(subparts[1].value))
		subparts[0].value->push_back(std::move(instr));
	return std::move(subparts[0].value);
}
std::unique_ptr<std::deque<std::unique_ptr<Instruction>>> Parser::reduce_EMPTY(std::deque<Token>) { 
	// <program> ::=
	return std::make_unique<std::deque<std::unique_ptr<Instruction>>>();
}
std::unique_ptr<std::deque<std::unique_ptr<Instruction>>> Parser::reduce_OPERATION(std::deque<Token> subparts) { 
	// <instruction> ::= "PLUS" | "MINUS" | ... | "COMMA"
	return std::move(subparts[0].value);
}
std::unique_ptr<std::deque<std::unique_ptr<Instruction>>> Parser::reduce_LOOP(std::deque<Token> subparts) {
	// <instruction> ::= "LBRACKET" <program> "RBRACKET"
    auto res = std::make_unique<std::deque<std::unique_ptr<Instruction>>>();
	res->push_back(std::make_unique<InstructionLoop>(std::move(*subparts[1].value.release())));
    return res;
	//std::cout << "PARSING: found LOOP" << std::endl;	
	//for(auto& s : *(subparts[1].value)) std::cout << "LOOP: " << (int)s << std::endl;
}


} // namespace bf
