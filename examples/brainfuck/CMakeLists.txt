# Parsodus - A language agnostic parser generator
# Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

add_custom_command(DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/bfLexer.lxs"
    COMMAND "${LEXESIS_EXE}" ARGS -d "${CMAKE_CURRENT_BINARY_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}/bfLexer.lxs"
    OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/BfLexer.h" "${CMAKE_CURRENT_BINARY_DIR}/BfLexer.cpp")

find_program(PARSODUS_EXE Parsodus PATH "${CMAKE_CURRENT_BINARY_DIR}/../../bin")

add_custom_command(DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/bfParser.pds"
    COMMAND "${PARSODUS_EXE}" ARGS -d "${CMAKE_CURRENT_BINARY_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}/bfParser.pds"
    OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/bfParser.h" "${CMAKE_CURRENT_BINARY_DIR}/bfParser.cpp")

include_directories("${CMAKE_CURRENT_BINARY_DIR}")

add_executable(bf
    EXCLUDE_FROM_ALL
    main.cpp
    parser.cpp
	generator.cpp
    "${CMAKE_CURRENT_BINARY_DIR}/BfLexer.cpp"
    "${CMAKE_CURRENT_BINARY_DIR}/bfParser.cpp")
