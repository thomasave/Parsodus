/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef CALC_PARSER_H
#define CALC_PARSER_H

#include "calcParser.h"
#include "CalcLexer.h"
#include "AST.h"

#include <map>
#include <memory>
#include <string>

namespace calc {

class Parser : public calcParser<std::unique_ptr<AST>> {
    public:
        Parser(CalcLexer lex);

    protected:
        using Value = std::unique_ptr<AST>;
        Token lex() override;
        Value error(Token current, const std::vector<calcParser_Symbol>& expected) override;
        Value reduce_arguments(std::deque<Token> subparts) override;
        Value reduce_assign(std::deque<Token> subparts) override;
        Value reduce_binop(std::deque<Token> subparts) override;
        Value reduce_expr_simple(std::deque<Token> subparts) override;
        Value reduce_functioncall(std::deque<Token> subparts) override;
        Value reduce_functiondef(std::deque<Token> subparts) override;
        Value reduce_idents(std::deque<Token> subparts) override;
        Value reduce_opt_arguments(std::deque<Token> subparts) override;
        Value reduce_opt_idents(std::deque<Token> subparts) override;
        Value reduce_parenthesized(std::deque<Token> subparts) override;
        Value reduce_toplevel(std::deque<Token> subparts) override;
        Value reduce_umin(std::deque<Token> subparts) override;

    private:
        CalcLexer m_lex;
        std::map<std::string, double> m_variables;
        std::map<std::string, std::unique_ptr<AST>> m_functions;
};

} /* calc  */ 
#endif //CALC_PARSER_H
