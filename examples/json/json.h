/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef JSON_H
#define JSON_H

#include <deque>
#include <iostream>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>

namespace json {

class JSONError : public std::runtime_error {
    public:
        JSONError(std::string msg) : std::runtime_error(msg) { }
};

enum Type {
    Num,
    String,
    Object,
    Array,
    Bool,
    Null
};

class JSON {
    public:
        /**
         * Default constructor, is a JSON null value
         */
        JSON();

        JSON(const JSON& other);
        JSON(JSON&& other);
        JSON& operator=(const JSON& other);
        JSON& operator=(JSON&& other);

        ~JSON();

        operator std::string() const;
        operator double() const;
        operator bool() const;
        JSON& operator[] (int) const;
        JSON& operator[] (std::string) const;
        JSON& operator[] (const char*) const;
        JSON& operator[] (const JSON&) const;

        std::string to_string() const;
        double to_double() const;
        bool to_bool() const;

        std::size_t size() const;
        void push_back(const JSON& other);
        void push_front(const JSON& other);

        bool isNull() const;

        Type getType() const;

        static JSON num(double n);
        static JSON string(const std::string& s);
        static JSON object();
        static JSON array();
        static JSON boolean(bool b);
        static JSON null();

        friend std::ostream& operator<<(std::ostream&, const JSON&);

    private:
        Type type;

        //Pointers because of union restrictions
        union Value {
            double num;
            std::string* str;
            std::map<std::string, JSON>* obj;
            std::deque<JSON>* arr;
            bool b;
        } val;

        JSON(Type t);
        JSON(Type t, Value v);
};

std::ostream& operator<<(std::ostream& os, const JSON& j);

}

#endif
