#!/usr/bin/python3

# Parsodus - A language agnostic parser generator
# Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import unittest, subprocess, filecmp, argparse, os.path, os, shutil

REFERENCE_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "test_data")

JSON_TESTS = [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1]
BF_TESTS = [0, 0, 0, 0, 0, 0]

def make_json_test(idx, should_fail=0):
    def test(self):
        inpath = os.path.join(REFERENCE_DIR, "json%03d.json" % idx)
        infile = open(inpath)
        outpath1 = os.path.join(data_dir, "json%03d.out1.json" % idx)
        outfile1 = open(outpath1, "w")
        outpath2 = os.path.join(data_dir, "json%03d.out2.json" % idx)
        outfile2 = open(outpath2, "w")
        p = subprocess.Popen([os.path.join(args.builddir, "examples", "json", "json")], stdin=infile, stdout=outfile1, stderr=subprocess.DEVNULL)
        p.communicate()
        infile.close()
        outfile1.close()

        if should_fail == 1:
            outfile2.close()
            self.assertNotEqual(0, p.returncode)
            return
        self.assertEqual(0, p.returncode)

        outfile1 = open(outpath1)
        p = subprocess.Popen([os.path.join(args.builddir, "examples", "json", "json")], stdin=outfile1, stdout=outfile2, stderr=subprocess.DEVNULL)
        p.communicate()
        outfile1.close()
        outfile2.close()
        if should_fail == 2:
            self.assertNotEqual(0, p.returncode)
            return
        self.assertEqual(0, p.returncode)

        self.assertTrue(filecmp.cmp(outpath1, outpath2), "Testcase %d for json example failed" % idx)

    return test

def make_bf_test(idx, should_fail=False):
    def test(self):
        argpath = os.path.join(REFERENCE_DIR, "bf%03d.bf" % idx)
        inpath = os.path.join(REFERENCE_DIR, "bf%03d.in" % idx)
        infile = open(inpath)
        exppath = os.path.join(REFERENCE_DIR, "bf%03d.exp" % idx)
        outpath = os.path.join(data_dir, "bf%03d.out" % idx)
        outfile = open(outpath, "w")

        p = subprocess.Popen([os.path.join(args.builddir, "examples", "brainfuck", "bf"), argpath], stdin=infile, stdout=outfile, stderr=subprocess.DEVNULL)
        p.communicate()
        infile.close()
        outfile.close()

        if should_fail:
            self.assertNotEqual(0, p.returncode)
            return
        self.assertEqual(0, p.returncode)
        self.assertTrue(filecmp.cmp(exppath, outpath), "Testcase %d for bf example failed" % idx)

    return test

class Tests(unittest.TestCase):
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--builddir", metavar="builddir", default=os.path.join(os.path.abspath(os.path.dirname(__file__)), "build"), required=False)
    args = parser.parse_args()

    data_dir = os.path.join(args.builddir, "test_data_out")
    try:
        shutil.rmtree(data_dir)
    except Exception as err:
        pass
    os.mkdir(data_dir)

    for i, should_fail in enumerate(JSON_TESTS):
        setattr(Tests, "test_json_%03d" % (i + 1), make_json_test(i + 1, should_fail))

    for i, should_fail in enumerate(BF_TESTS):
        setattr(Tests, "test_bf_%03d" % (i + 1), make_bf_test(i + 1, should_fail))

    unittest.main()
