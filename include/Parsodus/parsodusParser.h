/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef PARSODUS_PARSER_parsodusParser_H
#define PARSODUS_PARSER_parsodusParser_H

#include <cassert>
#include <cstdint>
#include <deque>
#include <stack>
#include <stdexcept>
#include <vector>

/**
 * Represents the type of the symbol (both terminals and nonterminals)
 */
enum class parsodusParser_Symbol : std::uint64_t {
    T_EOF,
    T_ARROW,
    T_COLON,
    T_COMMA,
    T_GRAMMAR,
    T_LBRACKET,
    T_LEFT,
    T_LEXESIS,
    T_LEXESISNAME,
    T_NONASSOC,
    T_NUM,
    T_PARSER,
    T_PARSERTYPE,
    T_PIPE,
    T_PRECEDENCE,
    T_RBRACKET,
    T_RIGHT,
    T_RULENAME,
    T_SEMICOLON,
    T_START,
    T_TERMINAL,
    T_TERMINALS,
    T_VARIABLE,
    V_bodies,
    V_body,
    V_error,
    V_opt_prec,
    V_precedence,
    V_precedences,
    V_rule,
    V_rules,
    V_section,
    V_sections,
    V_term_var,
    V_terminals,
};

class SyntaxError : public std::runtime_error {
public:
    SyntaxError(const char* c) : std::runtime_error(c) {}
};

template <typename Value>
class parsodusParser {
    public:
        parsodusParser() {}
        virtual ~parsodusParser() {}

        /**
         * Parse it
         */
        Value parse();

    protected:
        /**
         * A token, consisting of a Symbol type (should be a terminal) and a Value
         */
        struct Token {
            Token(const parsodusParser_Symbol& sym, const Value& val) : symbol(sym), value(val) {}
            Token(const parsodusParser_Symbol& sym, Value&& val) : symbol(sym), value(std::move(val)) {}
            parsodusParser_Symbol symbol;
            Value value;
        };


        /******************************************
        *  Functions to be supplied by the user  *
        ******************************************/

        /**
         * Handle an error
         * current is the current Token, one that has no action associated in the current state
         * expected is a listing of all terminals that do have an action
         *
         * By default throws an error
         */
        virtual Value error(Token current, const std::vector<parsodusParser_Symbol>& expected);

        /**
         * Get the next token from the lexer
         */
        virtual Token lex() = 0;
        
        /**
         * Apply a reduction (a grammar rule in reverse)
         */
        virtual Value reduce_0(std::deque<Token> subparts) = 0;
        virtual Value reduce_1(std::deque<Token> subparts) = 0;
        virtual Value reduce_10(std::deque<Token> subparts) = 0;
        virtual Value reduce_11(std::deque<Token> subparts) = 0;
        virtual Value reduce_12(std::deque<Token> subparts) = 0;
        virtual Value reduce_13(std::deque<Token> subparts) = 0;
        virtual Value reduce_14(std::deque<Token> subparts) = 0;
        virtual Value reduce_15(std::deque<Token> subparts) = 0;
        virtual Value reduce_16(std::deque<Token> subparts) = 0;
        virtual Value reduce_17(std::deque<Token> subparts) = 0;
        virtual Value reduce_18(std::deque<Token> subparts) = 0;
        virtual Value reduce_19(std::deque<Token> subparts) = 0;
        virtual Value reduce_2(std::deque<Token> subparts) = 0;
        virtual Value reduce_20(std::deque<Token> subparts) = 0;
        virtual Value reduce_21(std::deque<Token> subparts) = 0;
        virtual Value reduce_22(std::deque<Token> subparts) = 0;
        virtual Value reduce_23(std::deque<Token> subparts) = 0;
        virtual Value reduce_24(std::deque<Token> subparts) = 0;
        virtual Value reduce_25(std::deque<Token> subparts) = 0;
        virtual Value reduce_26(std::deque<Token> subparts) = 0;
        virtual Value reduce_3(std::deque<Token> subparts) = 0;
        virtual Value reduce_4(std::deque<Token> subparts) = 0;
        virtual Value reduce_5(std::deque<Token> subparts) = 0;
        virtual Value reduce_6(std::deque<Token> subparts) = 0;
        virtual Value reduce_7(std::deque<Token> subparts) = 0;
        virtual Value reduce_8(std::deque<Token> subparts) = 0;
        virtual Value reduce_9(std::deque<Token> subparts) = 0;

    private:
};

template <>
class parsodusParser<bool> {
    public:
        parsodusParser() {}
        virtual ~parsodusParser() {}

        /**
         * Parse it
         */
        bool parse();

    protected:
        /******************************************
        *  Functions to be supplied by the user  *
        ******************************************/

        /**
         * Get the next token from the lexer
         */
        virtual parsodusParser_Symbol lex() = 0;
};

#define TABLE parsodusParser___Table___parsodusParser
#define REDUCE_COUNT parsodusParser___Num_Reduces___parsodusParser
// Not a static member because the table should not be replicated for different instantiations of the parser
extern const std::uint64_t TABLE[49][35];
extern const unsigned char REDUCE_COUNT[27];

enum Action {
    ERROR = 0,
    SHIFT = 1,
    REDUCE = 2,
    ACCEPT = 3
};


/*********************************************
*  Translate a Symbol to a readable string  *
*********************************************/
inline std::string to_string(parsodusParser_Symbol s) {
    switch (s) {
        case parsodusParser_Symbol::T_EOF:
            return "T_EOF";
        case parsodusParser_Symbol::T_ARROW:
            return "T_ARROW";
        case parsodusParser_Symbol::T_COLON:
            return "T_COLON";
        case parsodusParser_Symbol::T_COMMA:
            return "T_COMMA";
        case parsodusParser_Symbol::T_GRAMMAR:
            return "T_GRAMMAR";
        case parsodusParser_Symbol::T_LBRACKET:
            return "T_LBRACKET";
        case parsodusParser_Symbol::T_LEFT:
            return "T_LEFT";
        case parsodusParser_Symbol::T_LEXESIS:
            return "T_LEXESIS";
        case parsodusParser_Symbol::T_LEXESISNAME:
            return "T_LEXESISNAME";
        case parsodusParser_Symbol::T_NONASSOC:
            return "T_NONASSOC";
        case parsodusParser_Symbol::T_NUM:
            return "T_NUM";
        case parsodusParser_Symbol::T_PARSER:
            return "T_PARSER";
        case parsodusParser_Symbol::T_PARSERTYPE:
            return "T_PARSERTYPE";
        case parsodusParser_Symbol::T_PIPE:
            return "T_PIPE";
        case parsodusParser_Symbol::T_PRECEDENCE:
            return "T_PRECEDENCE";
        case parsodusParser_Symbol::T_RBRACKET:
            return "T_RBRACKET";
        case parsodusParser_Symbol::T_RIGHT:
            return "T_RIGHT";
        case parsodusParser_Symbol::T_RULENAME:
            return "T_RULENAME";
        case parsodusParser_Symbol::T_SEMICOLON:
            return "T_SEMICOLON";
        case parsodusParser_Symbol::T_START:
            return "T_START";
        case parsodusParser_Symbol::T_TERMINAL:
            return "T_TERMINAL";
        case parsodusParser_Symbol::T_TERMINALS:
            return "T_TERMINALS";
        case parsodusParser_Symbol::T_VARIABLE:
            return "T_VARIABLE";
        case parsodusParser_Symbol::V_bodies:
            return "V_bodies";
        case parsodusParser_Symbol::V_body:
            return "V_body";
        case parsodusParser_Symbol::V_error:
            return "V_error";
        case parsodusParser_Symbol::V_opt_prec:
            return "V_opt_prec";
        case parsodusParser_Symbol::V_precedence:
            return "V_precedence";
        case parsodusParser_Symbol::V_precedences:
            return "V_precedences";
        case parsodusParser_Symbol::V_rule:
            return "V_rule";
        case parsodusParser_Symbol::V_rules:
            return "V_rules";
        case parsodusParser_Symbol::V_section:
            return "V_section";
        case parsodusParser_Symbol::V_sections:
            return "V_sections";
        case parsodusParser_Symbol::V_term_var:
            return "V_term_var";
        case parsodusParser_Symbol::V_terminals:
            return "V_terminals";
    }
}


/**************************
*  Default error method  *
**************************/
template <typename Value>
Value parsodusParser<Value>::error(Token current, const std::vector<parsodusParser_Symbol>& expected) {
    std::string msg = "Syntax Error: got " + to_string(current.symbol) + "\n    Expected any of:";
    for (auto& s : expected) {
        msg += "\n        " + to_string(s);
    }
    throw SyntaxError(msg.c_str());
}


/***************************
*  Parser implementation  *
***************************/
template <typename Value>
Value parsodusParser<Value>::parse() {
    std::stack<Token> valueStack;
    std::stack<std::uint64_t> stateStack;

    stateStack.push(0);
    Token tok = lex();

    while (true) {
        std::uint64_t act = TABLE[stateStack.top()][static_cast<std::uint64_t>(tok.symbol)];

       switch (act & 0x3) {
            case ERROR:
                {
                    constexpr std::uint64_t verr = static_cast<std::uint64_t>(parsodusParser_Symbol::V_error);
                    std::vector<parsodusParser_Symbol> expected;
                    std::uint64_t top = stateStack.top();
                    for (std::uint64_t i = 0; i <= static_cast<std::uint64_t>(parsodusParser_Symbol::T_VARIABLE); i++) {
                        if ((TABLE[top][i] & 0x3) != ERROR)
                            expected.emplace_back(static_cast<parsodusParser_Symbol>(i));
                    }
                    Token report = Token{tok.symbol, std::move(tok.value)};
                    Value errorVal = error(std::move(report), expected);

                    while (!valueStack.empty() && (TABLE[stateStack.top()][verr] & 0x3) == ERROR) {
                        valueStack.pop();
                        stateStack.pop();
                    }
                    if ((TABLE[stateStack.top()][verr] & 0x3) == ERROR) {
                        throw SyntaxError("Syntax error: could not recover");
                    }

                    stateStack.push(TABLE[stateStack.top()][verr] >> 2);
                    valueStack.emplace(Token{ parsodusParser_Symbol::V_error, std::move(errorVal)});

                    while (tok.symbol != parsodusParser_Symbol::T_EOF && (TABLE[stateStack.top()][static_cast<std::uint64_t>(tok.symbol)] & 0x3) == ERROR) {
                        tok = lex();
                    }
                    if ((TABLE[stateStack.top()][static_cast<std::uint64_t>(tok.symbol)] & 0x3) == ERROR) {
                        throw SyntaxError("Syntax error: could not recover");
                    }
                }
                break;
            case SHIFT:
                valueStack.emplace(std::move(tok));
                stateStack.push(act >> 2);
                tok = lex();
                break;
            case REDUCE:
                {
                    std::uint64_t tmp = act >> 2;
                    parsodusParser_Symbol symbol = static_cast<parsodusParser_Symbol>(tmp >> 31);
                    std::uint32_t rule = tmp & ((1ull << 31) - 1);

                    std::deque<Token> dq;
                    for (unsigned char i = 0; i < REDUCE_COUNT[rule]; i++) {
                        dq.emplace_front(std::move(valueStack.top()));
                        valueStack.pop();
                        stateStack.pop();
                    }

                    switch (rule) {
                        case 0:
                            
                            valueStack.emplace(symbol, reduce_0(std::move(dq)));
                            break;
                        case 1:
                            
                            valueStack.emplace(symbol, reduce_1(std::move(dq)));
                            break;
                        case 2:
                            
                            valueStack.emplace(symbol, reduce_2(std::move(dq)));
                            break;
                        case 3:
                            
                            valueStack.emplace(symbol, reduce_3(std::move(dq)));
                            break;
                        case 4:
                            
                            valueStack.emplace(symbol, reduce_4(std::move(dq)));
                            break;
                        case 5:
                            
                            valueStack.emplace(symbol, reduce_5(std::move(dq)));
                            break;
                        case 6:
                            
                            valueStack.emplace(symbol, reduce_6(std::move(dq)));
                            break;
                        case 7:
                            
                            valueStack.emplace(symbol, reduce_7(std::move(dq)));
                            break;
                        case 8:
                            
                            valueStack.emplace(symbol, reduce_8(std::move(dq)));
                            break;
                        case 9:
                            
                            valueStack.emplace(symbol, reduce_9(std::move(dq)));
                            break;
                        case 10:
                            
                            valueStack.emplace(symbol, reduce_10(std::move(dq)));
                            break;
                        case 11:
                            
                            valueStack.emplace(symbol, reduce_11(std::move(dq)));
                            break;
                        case 12:
                            
                            valueStack.emplace(symbol, reduce_12(std::move(dq)));
                            break;
                        case 13:
                            
                            valueStack.emplace(symbol, reduce_13(std::move(dq)));
                            break;
                        case 14:
                            
                            valueStack.emplace(symbol, reduce_14(std::move(dq)));
                            break;
                        case 15:
                            
                            valueStack.emplace(symbol, reduce_15(std::move(dq)));
                            break;
                        case 16:
                            
                            valueStack.emplace(symbol, reduce_16(std::move(dq)));
                            break;
                        case 17:
                            
                            valueStack.emplace(symbol, reduce_17(std::move(dq)));
                            break;
                        case 18:
                            
                            valueStack.emplace(symbol, reduce_18(std::move(dq)));
                            break;
                        case 19:
                            
                            valueStack.emplace(symbol, reduce_19(std::move(dq)));
                            break;
                        case 20:
                            
                            valueStack.emplace(symbol, reduce_20(std::move(dq)));
                            break;
                        case 21:
                            
                            valueStack.emplace(symbol, reduce_21(std::move(dq)));
                            break;
                        case 22:
                            
                            valueStack.emplace(symbol, reduce_22(std::move(dq)));
                            break;
                        case 23:
                            
                            valueStack.emplace(symbol, reduce_23(std::move(dq)));
                            break;
                        case 24:
                            
                            valueStack.emplace(symbol, reduce_24(std::move(dq)));
                            break;
                        case 25:
                            
                            valueStack.emplace(symbol, reduce_25(std::move(dq)));
                            break;
                        case 26:
                            
                            valueStack.emplace(symbol, reduce_26(std::move(dq)));
                            break;
                        default:
                            assert(false); //There should be no such rule
                            break;
                    }

                    stateStack.push(TABLE[stateStack.top()][static_cast<std::uint64_t>(valueStack.top().symbol)] >> 2);
                }
                break;
            case ACCEPT:
                assert(stateStack.size() == 2);
                assert(valueStack.size() == 1);
                return std::move(valueStack.top().value);
            default:
                //IMPOSSIBLE
                break;
        }
    }
}

#undef REDUCE_COUNT
#undef TABLE

#endif /* PARSODUS_PARSER_parsodusParser_H */
