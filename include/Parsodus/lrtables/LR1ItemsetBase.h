/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef PARSODUS_LRTABLES_LR1ITEMSETBASE_H_EREKWQSM
#define PARSODUS_LRTABLES_LR1ITEMSETBASE_H_EREKWQSM

#include "Parsodus/lrtables/LR1Item.h"
#include "Parsodus/util/symbols.h"

#include <algorithm>
#include <ostream>
#include <set>

namespace pds {
namespace lr {

template <typename Itemset>
class LR1ItemsetBase {
public:
    LR1ItemsetBase();
    LR1ItemsetBase(std::shared_ptr<Rule> start);

    static bool needsFollow();

    void close(const Grammar& g);
    Itemset succ(std::string sym) const;
    bool operator==(const Itemset& rhs) const;
    bool empty() const;
    std::set<std::size_t> getReduces(const Grammar& g, std::string lookahead) const;

    friend std::ostream& operator<<(std::ostream& os, const LR1ItemsetBase<Itemset>& its) {
        for (const auto& it : its.m_items) {
            os << it.rule->head << " ->";
            for (std::size_t i = 0; i < it.rule->tail.size(); i++) {
                if (i == it.dotIdx)
                    os << " ·";
                os << " " << it.rule->tail[i];
            }
            if (it.dotIdx == it.rule->tail.size()) 
                os << " ·";

            os << "  [";
            for (auto la = it.lookaheads.cbegin(); la != it.lookaheads.cend(); la++) {
                if (la != it.lookaheads.cbegin())
                    os << ", ";
                os << *la;
            }
            os << "]" << std::endl;
        }
        return os;
    }

protected:
    std::vector<LR1Item> m_items;
};

template <typename Itemset>
LR1ItemsetBase<Itemset>::LR1ItemsetBase()
{}

template <typename Itemset>
LR1ItemsetBase<Itemset>::LR1ItemsetBase(std::shared_ptr<Rule> start) {
    m_items.emplace_back(LR1Item{start, 0, {util::EOF_PLACEHOLDER}});
}

template <typename Itemset>
bool LR1ItemsetBase<Itemset>::needsFollow() {
    return true;
}

template <typename Itemset>
void LR1ItemsetBase<Itemset>::close(const Grammar& g) {
    bool changes = true;
    std::vector<LR1Item> todo;
    std::set<std::pair<std::string, std::string>> added; // (variable, lookahead)

    while (changes) {
        changes = false;
        std::set<LR1Item> toAdd;

        for (const LR1Item& i : m_items) {
            if (i.dotIdx < i.rule->tail.size()) {
                std::string& sym = i.rule->tail[i.dotIdx];
                if (g.variables.count(sym)) {
                    std::vector<std::string> seq(std::vector<std::string>(i.rule->tail.begin() + i.dotIdx + 1, i.rule->tail.end()));
                    std::set<std::string> first = (*g.first)(seq);
                    if (first.count("") || !first.size()) {
                        first.insert(i.lookaheads.begin(), i.lookaheads.end());
                        first.erase("");
                    }

                    for (std::string newLookahead : first) {
                        if (!added.count({sym, newLookahead})) {
                            added.emplace(sym, newLookahead);
                            changes = true;
                            for (const auto& rule : g.rules) {
                                if (rule->head == sym) {
                                    toAdd.insert(LR1Item{rule, 0, {newLookahead}});
                                }
                            }
                        }
                    }
                }
            }
        }

        for (const auto& newItem : toAdd) {
            bool found = false;
            for (auto& oldItem : m_items) {
                if (newItem.dotIdx == oldItem.dotIdx && newItem.rule == oldItem.rule) {
                    found = true;
                    oldItem.lookaheads.insert(newItem.lookaheads.begin(), newItem.lookaheads.end());
                    break;
                }
            }
            if (!found) {
                m_items.push_back(newItem);
            }
        }
    }
}

template <typename Itemset>
Itemset LR1ItemsetBase<Itemset>::succ(std::string sym) const {
    Itemset sc;
    for (auto& item : m_items) {
        if (item.dotIdx < item.rule->tail.size()) {
            if (item.rule->tail[item.dotIdx] == sym) {
                sc.m_items.push_back(LR1Item{item.rule, item.dotIdx + 1, item.lookaheads});
            }
        }
    }
    return sc;
}

template <typename Itemset>
bool LR1ItemsetBase<Itemset>::operator==(const Itemset& rhs) const {
    return m_items == rhs.m_items;
}

template <typename Itemset>
bool LR1ItemsetBase<Itemset>::empty() const {
    return m_items.empty();
}

template <typename Itemset>
std::set<std::size_t> LR1ItemsetBase<Itemset>::getReduces(const Grammar& g, std::string lookahead) const {
    std::set<std::size_t> result;
    for (const auto& item : m_items) {
        if (item.dotIdx >= item.rule->tail.size() && item.lookaheads.count(lookahead)) {
            result.insert(std::find(g.rules.begin(), g.rules.end(), item.rule) - g.rules.begin());
        }
    }
    return result;
}

} /* lr */ 
} /* pds */

#endif /* PARSODUS_LRTABLES_LR1ITEMSETBASE_H_EREKWQSM */
