/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef PARSODUS_LRTABLES_GENERATOR_H_YW3GIUNH
#define PARSODUS_LRTABLES_GENERATOR_H_YW3GIUNH

#include "Parsodus/grammar.h"
#include "Parsodus/util/symbols.h"
#include "Parsodus/lrtables/table.h"

#include "g3log/g3log.hpp"

#include <algorithm>
#include <cassert>
#include <memory>
#include <queue>

namespace pds {
namespace lr {

/**
 * Base class for LR (and derivative) table generators (such as SLR and LALR)
 * Parametrized on the type of itemset (configuration set) to be used
 *
 * An Itemset should support:
 * - A constructor taking a single Rule, that makes this a starting rule
 * - void close(const Grammar&); compute the closure
 * - Itemset succ(std::string) const; compute the successor of this set, over the given symbol
 * - bool operator==(const Itemset&); are these two Itemsets equal
 * - bool canMerge(const Itemset&) const; Can the given Itemset be merged into this one
 * - bool merge(const Itemset&); Merge the given Itemset into this one, return whether there was a change
 * - bool empty() const; is this Itemset empty (== not useful)
 * - std::set<std::size_t> getReduces(const Grammar&, std::string) const; get all Rule indices where a reduce should happen with given lookahead (not necessarily a set, but iterable)
 * - static bool needsFollow() const; does this type of Itemset need Follow sets to work, if so the first and follow unique_ptr's of the grammar passed will be initialized
 */
template <typename Itemset>
class Generator {
    public:
        /**
         * Constructor
         *
         * @param start The start symbol for the grammar
         * @param g The grammar to translate
         */
        Generator(const Grammar& g);

        /**
         * Generate an LRTable based on given grammar
         *
         * @returns An LR (or derivative) table for the grammar given at construction
         */
        LRTable generate();

    private:
        Grammar m_gram;
        std::shared_ptr<Rule> m_startrule;
};

template <typename Itemset>
Generator<Itemset>::Generator(const Grammar& g) : m_gram(g), m_startrule(std::make_shared<Rule>(util::EXTENDED_START, std::vector<std::string>{g.start})) {
    m_gram.terminals.insert(util::EOF_PLACEHOLDER); //End of file
    m_gram.variables.insert(util::EXTENDED_START); //modified start rule
    m_gram.rules.push_back(m_startrule);
    if (Itemset::needsFollow()) {
        m_gram.first = std::make_unique<util::FirstSet>(m_gram);
        m_gram.follow = std::make_unique<util::FollowSet>(m_gram, *m_gram.first);
    }
}

template <typename Itemset>
LRTable Generator<Itemset>::generate() {
    LRTable table;

    //Start with size 1
    table.act.emplace_back();
    table.goto_.emplace_back();

    std::vector<Itemset> itemsets;
    itemsets.emplace_back(Itemset(m_startrule));
    itemsets[0].close(m_gram);

    std::set<std::string> symbols;
    std::set_union(m_gram.terminals.begin(), m_gram.terminals.end(),
                    m_gram.variables.begin(), m_gram.variables.end(),
                    std::inserter(symbols, symbols.end()));

    std::map<std::size_t, std::map<std::string, std::pair<Action, std::size_t>>> errors;
    bool reduceReduce = false;

    std::queue<std::pair<std::size_t, Itemset>> q;
    q.emplace(0, itemsets[0]);
    while (!q.empty()) {
        auto& curP = q.front();
        std::size_t curIdx = curP.first;
        Itemset cur = curP.second;
        q.pop();

        for (const std::string& sym : symbols) {
            Itemset s = cur.succ(sym);
            if (s.empty())
                continue;
            s.close(m_gram);
            
            std::size_t idx;
            for (idx = 0; idx < itemsets.size(); idx++) {
                if (itemsets[idx] == s) {
                    break;
                } else if (itemsets[idx].canMerge(s)) {
                    if (itemsets[idx].merge(s)) {
                        q.emplace(idx, std::move(s));
                    }
                    break;
                }
            }
            if (idx == itemsets.size()) {
                q.emplace(idx, s);
                itemsets.emplace_back(std::move(s));
                
                //Grow the table
                table.act.emplace_back();
                table.goto_.emplace_back();
            }

            if (m_gram.variables.count(sym)) {
                table.goto_[curIdx][sym] = idx;
            } else {
                table.act[curIdx][sym] = std::make_pair(Action::SHIFT, idx);
            }

        }
        
        for (std::string term : m_gram.terminals) {
            //Get reduces from the itemset, add them to the table, look for conflicts
            for (std::size_t rule_applied : cur.getReduces(m_gram, term)) {
                if (rule_applied == m_gram.rules.size() - 1) { // The last added rule
                    // The extended start rule
                    if (term == util::EOF_PLACEHOLDER)
                        table.act[curIdx][term] = std::make_pair(Action::ACCEPT, 0);
                } else if (table.act[curIdx].count(term)) {
                    if (table.act[curIdx][term].first == Action::SHIFT) {
                        bool handled = false;

                        auto rightTokenIt = m_gram.precedence.find(term);
                        if (m_gram.rules[rule_applied]->precedence.first && rightTokenIt != m_gram.precedence.end()) {
                            auto& leftPrec = m_gram.rules[rule_applied]->precedence.second;
                            if (leftPrec.first < rightTokenIt->second.first) {
                                // Keep the shift
                                handled = true;
                            } else if (leftPrec.first > rightTokenIt->second.first) {
                                // replace with a reduce
                                table.act[curIdx][term] = {Action::REDUCE, rule_applied};
                                handled = true;
                            } else if (leftPrec.second == PrecedenceType::LEFT) {
                                // replace with a reduce
                                table.act[curIdx][term] = {Action::REDUCE, rule_applied};
                                handled = true;
                            } else if (leftPrec.second == PrecedenceType::RIGHT) {
                                // Keep the shift
                                handled = true;
                            } else {
                                //make it an error
                                table.act[curIdx][term] = {Action::ERROR, 0};
                                handled = true;
                            }
                        }
                        if (!handled) {
                            LOG(WARNING) << "Shift/reduce conflict in state " << curIdx << ", favouring shift" << std::endl;
                            errors[curIdx][term] = {Action::REDUCE, rule_applied};
                        }
                    } else if (table.act[curIdx][term].first == Action::REDUCE
                            && table.act[curIdx][term].second != rule_applied) {
                        LOG(WARNING) << "Reduce/reduce conflict in state " << curIdx << std::endl;
                        errors[curIdx][term] = {Action::REDUCE, rule_applied};
                        reduceReduce = true;
                    } else {
                        //Reduce using the same rule, no problem, NO-OP
                    }
                } else {
                    // No conflicts
                    table.act[curIdx][term] = std::make_pair(Action::REDUCE, rule_applied);
                }
            }
        }
    }

    auto reporter = [&table](std::size_t idx, std::string sym, const std::pair<Action, std::size_t>& p) -> std::string {
        switch (p.first) {
            case Action::ERROR:
                return "on " + sym + " fail\n";
            case Action::SHIFT:
                return "on " + sym + " shift and go to " + std::to_string(table.act[idx][sym].second) + "\n";
            case Action::REDUCE:
                return "on " + sym + " reduce with rule " + std::to_string(table.act[idx][sym].second) + "\n";
            case Action::ACCEPT:
                return "on " + sym + " accept\n";
            default:
                return "";
        }
    };

    for (std::size_t idx = 0; idx < itemsets.size(); idx++) {
        LOG(INFO) << "State " << idx << std::endl
            << itemsets[idx] << std::endl;
        for (std::string terminal : m_gram.terminals) {
            auto pIt = table.act[idx].find(terminal);
            if (pIt != table.act[idx].end()) {
                LOG(INFO) << reporter(idx, terminal, pIt->second);
            } else {
                LOG(INFO) << reporter(idx, terminal, {Action::ERROR, 0});
            }
        }
        LOG(INFO) << std::endl;
        bool doneGoto = false, doneError = false;
        for (std::string var : m_gram.variables) {
            if (table.goto_[idx].count(var)) {
                LOG(INFO) << "after " << var << " go to state " << table.goto_[idx][var] << std::endl;
                doneGoto = true;
            }
        }
        if (doneGoto)
            LOG(INFO) << std::endl;
        for (auto& p : errors[idx]) {
            doneError = true;
            LOG(INFO) << "! " << reporter(idx, p.first, p.second);
        }
        if (doneError)
            LOG(INFO) << std::endl;
        LOG(INFO) << std::string(50, '=') << std::endl << std::endl;
    }

    if (reduceReduce) {
        throw std::runtime_error("Stopped generating because of errors in the file");
    }

    return table;
}

} /* lr */
} /* pds */

#endif /* PARSODUS_LRTABLES_GENERATOR_H_YW3GIUNH */
