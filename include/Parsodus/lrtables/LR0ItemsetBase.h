/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef PARSODUS_LRTABLES_LR0ITEMSETBASE_H_GGIPISTD
#define PARSODUS_LRTABLES_LR0ITEMSETBASE_H_GGIPISTD

#include "Parsodus/lrtables/LR0Item.h"

#include <ostream>
#include <set>

namespace pds {
namespace lr {

/**
 * Provide common methods for Itemsets based upon LR(0) items
 * (SLR(1) and LR(0))
 */
template <typename Itemset>
class LR0ItemsetBase {
public:
    LR0ItemsetBase();
    LR0ItemsetBase(std::shared_ptr<Rule> start);

    void close(const Grammar& g);
    Itemset succ(std::string sym) const;
    bool operator==(const Itemset& rhs) const;
    bool canMerge(const Itemset& rhs) const;
    bool merge(const Itemset& rhs);
    bool empty() const;

    friend std::ostream& operator<<(std::ostream& os, const LR0ItemsetBase<Itemset>& its) {
        for (const auto& it : its.m_items) {
            os << it.rule->head << " ->";
            for (std::size_t i = 0; i < it.rule->tail.size(); i++) {
                if (i == it.dotIdx)
                    os << " ·";
                os << " " << it.rule->tail[i];
            }
            if (it.dotIdx == it.rule->tail.size()) 
                os << " ·";
            os << std::endl;
        }
        return os;
    }

protected:
    std::set<LR0Item> m_items;
};

template <typename Itemset>
LR0ItemsetBase<Itemset>::LR0ItemsetBase()
{}

template <typename Itemset>
LR0ItemsetBase<Itemset>::LR0ItemsetBase(std::shared_ptr<Rule> start) {
    m_items.emplace(LR0Item{start, 0});
}

template <typename Itemset>
void LR0ItemsetBase<Itemset>::close(const Grammar& g) {
    bool changes = true;
    std::vector<LR0Item> todo;
    std::set<std::string> added;

    while (changes) {
        changes = false;
        std::set<LR0Item> toAdd;

        for (const LR0Item& i : m_items) {
            if (i.dotIdx < i.rule->tail.size()) {
                std::string& sym = i.rule->tail[i.dotIdx];
                if (g.variables.count(sym) && !added.count(sym)) {
                    added.insert(sym);
                    changes = true;
                    for (const auto& rule : g.rules) {
                        if (rule->head == sym) {
                            toAdd.insert(LR0Item{rule, 0});
                        }
                    }
                }
            }
        }
        m_items.insert(toAdd.begin(), toAdd.end());
    }
}

template <typename Itemset>
Itemset LR0ItemsetBase<Itemset>::succ(std::string sym) const {
    Itemset sc;
    for (auto& item : m_items) {
        if (item.dotIdx < item.rule->tail.size()) {
            if (item.rule->tail[item.dotIdx] == sym) {
                sc.m_items.insert(LR0Item{item.rule, item.dotIdx + 1});
            }
        }
    }
    return sc;
}

template <typename Itemset>
bool LR0ItemsetBase<Itemset>::operator==(const Itemset& rhs) const {
    return m_items == rhs.m_items;
}

template <typename Itemset>
bool LR0ItemsetBase<Itemset>::canMerge(const Itemset&) const {
    return false;
}

template <typename Itemset>
bool LR0ItemsetBase<Itemset>::merge(const Itemset&) {
    //NO-OP
    return false;
}

template <typename Itemset>
bool LR0ItemsetBase<Itemset>::empty() const {
    return m_items.empty();
}
    

} /* lr */ 
} /* pds */

#endif /* PARSODUS_LRTABLES_LR0ITEMSETBASE_H_GGIPISTD */
