/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef PARSODUS_LRTABLES_LR1ITEMSET_H_AFGBM4VN
#define PARSODUS_LRTABLES_LR1ITEMSET_H_AFGBM4VN

#include "Parsodus/lrtables/LR1ItemsetBase.h"

namespace pds {
namespace lr {

class LR1Itemset : public LR1ItemsetBase<LR1Itemset> {
public:
    LR1Itemset();
    LR1Itemset(std::shared_ptr<Rule> start);

    bool canMerge(const LR1Itemset& rhs) const;
    bool merge(const LR1Itemset& rhs);

private:
};

} /* lr */ 
} /* pds */

#endif /* PARSODUS_LRTABLES_LR1ITEMSET_H_AFGBM4VN */
