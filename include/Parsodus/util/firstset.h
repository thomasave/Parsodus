/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef PARSODUS_FIRSTSET_H_Q6U5VBG0
#define PARSODUS_FIRSTSET_H_Q6U5VBG0

#include <map>
#include <set>
#include <string>
#include <vector>

namespace pds {

struct Grammar;

namespace util {

class FirstSet {
    public:
        FirstSet(const Grammar& g);

        /**
         * Get the first set for the given symbol
         */
        std::set<std::string> operator()(std::string key) const;

        /**
         * Get the first set for the given sequence of symbols
         */
        std::set<std::string> operator()(std::vector<std::string> sequence) const;
        

    private:
        std::map<std::string, std::set<std::string>> m_first;
};

} /* util  */ 
} /* pds */

#endif /* PARSODUS_FIRSTSET_H_Q6U5VBG0 */
