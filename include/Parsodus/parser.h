/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef PARSODUS_PARSER_H
#define PARSODUS_PARSER_H

#include "ParsodusLexer.h"
#include "Parsodus/parsodusParser.h"
#include "config.h"
#include <deque>

namespace pds {

class Parser : public parsodusParser<std::unique_ptr<Config>> {
	public:
		Parser(ParsodusLexer lex);

	protected:
		Token lex() override;
        std::unique_ptr<Config> reduce_0(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_1(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_2(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_3(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_4(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_5(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_6(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_7(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_8(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_9(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_10(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_11(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_12(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_13(std::deque<Token> subparts) override;
        std::unique_ptr<Config> reduce_14(std::deque<Token> subparts) override;
		std::unique_ptr<Config> reduce_15(std::deque<Token> subparts) override;
		std::unique_ptr<Config> reduce_16(std::deque<Token> subparts) override;
		std::unique_ptr<Config> reduce_17(std::deque<Token> subparts) override;
		std::unique_ptr<Config> reduce_18(std::deque<Token> subparts) override;
		std::unique_ptr<Config> reduce_19(std::deque<Token> subparts) override;
		std::unique_ptr<Config> reduce_20(std::deque<Token> subparts) override;
		std::unique_ptr<Config> reduce_21(std::deque<Token> subparts) override;
		std::unique_ptr<Config> reduce_22(std::deque<Token> subparts) override;
		std::unique_ptr<Config> reduce_23(std::deque<Token> subparts) override;		
		std::unique_ptr<Config> reduce_24(std::deque<Token> subparts) override;		
		std::unique_ptr<Config> reduce_25(std::deque<Token> subparts) override;		
		std::unique_ptr<Config> reduce_26(std::deque<Token> subparts) override;		

	private:
		ParsodusLexer m_lex;
		int m_precedenceCounter = 0;

};

}
#endif // PARSODUS_PARSER_H
