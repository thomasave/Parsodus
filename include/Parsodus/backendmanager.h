/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef PARSODUS_BACKENDMANAGER_H
#define PARSODUS_BACKENDMANAGER_H

#include <memory>
#include <string>
#include <vector>

#include "Parsodus/backend.h"
#include "Parsodus/lrtables/generator.h"
#include "Parsodus/lrtables/LR0Itemset.h"
#include "Parsodus/lrtables/SLR1Itemset.h"
#include "Parsodus/lrtables/LR1Itemset.h"
#include "Parsodus/lrtables/LALR1Itemset.h"
#include "Parsodus/util/parserType.h"

namespace pds {
    /**
     * A manager for backends
     * Aggregates and allow to search for backends that can process a specific language
     */
    class BackendManager {
        public:
            /**
             * Add a backend to the list of registered backends
             *
             * @param backend The backend to register
             */
            void registerBackend(std::unique_ptr<Backend> backend);

            template<template<class > class T>
            void registerLR() {
                registerBackend(std::make_unique<T<lr::Generator<lr::LR0Itemset>>>("LR(0)"));
                registerBackend(std::make_unique<T<lr::Generator<lr::SLR1Itemset>>>("SLR(1)"));
                registerBackend(std::make_unique<T<lr::Generator<lr::LR1Itemset>>>("LR(1)"));
                registerBackend(std::make_unique<T<lr::Generator<lr::LALR1Itemset>>>("LALR(1)"));
            }
            
            /**
             * Get a backend that can process the given language
             * The manager retains ownership of the returned pointer
             *
             * @param lang The language the backend should be able to process
             * @param parserType the type of parser it should be able to produce (e.g. LALR_1, LR_0, etc..)
             * @returns A pointer to a Backend if it can find one, nullptr otherwise
             */
            Backend* findBackend(std::string lang, std::string parserType);

        private:
            std::vector<std::unique_ptr<Backend> > m_backends; ///< The list of registered backends
    };
}

#endif //PARSODUS_BACKENDMANAGER_H

