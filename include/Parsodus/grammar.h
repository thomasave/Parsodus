/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef PARSODUS_GRAMMAR_H

#include "Parsodus/util/firstset.h"
#include "Parsodus/util/followset.h"

#include <deque>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

namespace pds {
    enum class PrecedenceType {
        LEFT,
        RIGHT,
        NONASSOC
    };

    /**
     * Represents a grammar rule
     * head -> tail[0] tail[1] ... tail[tail.size() - 1]
     */
    struct Rule {
        std::string head; ///< The replaced variable
        std::vector<std::string> tail; ///< The replacement rule
        const std::string name; ///< An optional name for this rule, if it's empty, there's no name
        std::pair<bool, std::pair<int, PrecedenceType>> precedence; ///< precedence for this rule, the bool indicates whether it's valid (invalid if there is no rightmost terminal and no explicit precedence)
        bool operator<(const Rule& other) const {
            if(head != other.head){
                return head < other.head;
            } else {
                return tail < other.tail;
            }
        }

        Rule() : Rule("", {}, ""){}
        Rule(const std::string& h, const std::vector<std::string>& t) : Rule(h, t, "") {}
        Rule(const std::string& h, const std::vector<std::string>& t, const std::string& name) : head(h), tail(t), name(name), precedence{false, {0, PrecedenceType::RIGHT}} {}
        Rule(const std::string& h, const std::vector<std::string>& t, const std::string& name, std::pair<int, PrecedenceType> precedence) : 
            head(h), tail(t), name(name), precedence{true, precedence} {}
    };

    /**
     * A context free grammar
     * Keeps track of variables, terminals and replacement rules
     */
    struct Grammar {
        std::string start; ///< the starting variable
        std::set<std::string> variables; ///< the variables
        std::set<std::string> terminals; ///< the terminals
        std::deque<std::shared_ptr<Rule>> rules; ///< the replacement rules
        std::map<std::string, std::pair<int, PrecedenceType> > precedence; ///< higher value -> higher precedence

        std::unique_ptr<util::FirstSet> first;
        std::unique_ptr<util::FollowSet> follow;

        Grammar() : start(""), variables(), terminals(), rules(), precedence(), first(nullptr), follow(nullptr)
        {}

        Grammar(const Grammar& rhs)
            : start(rhs.start), variables(rhs.variables), terminals(rhs.terminals)
              , rules(rhs.rules), precedence(rhs.precedence), first(nullptr), follow(nullptr) {
                  if (rhs.first)
                      first = std::make_unique<util::FirstSet>(*rhs.first);
                  if (rhs.follow)
                      follow = std::make_unique<util::FollowSet>(*rhs.follow);
              }
    };
}

#endif //PARSODUS_GRAMMAR_H
