# Parsodus - A language agnostic parser generator
# Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

_Parsodus_completion ()
{
    local langs
    langs='c++ cpp cxx'

    local opts
    opts='-h --help --version -d --outputdir -l --language -n --name --debug'
    # local prev
    COMPREPLY=()
    local cur
    cur=${COMP_WORDS[COMP_CWORD]}
    local prev
    local pprev
    if [ $COMP_CWORD -gt 1 ]; then
        prev=${COMP_WORDS[ $(( $COMP_CWORD - 1 )) ]}
    fi
    if [ $COMP_CWORD -gt 2 ]; then
        pprev=${COMP_WORDS[ $(( $COMP_CWORD - 2 )) ]}
    fi

    case "$cur" in
        -*)
            COMPREPLY=( $(compgen -W "$opts" -- $cur) )
            ;;
        =*)
            case "$prev" in
                --language)
                    COMPREPLY=( $(compgen -W "$langs" ))
                    ;;
                --outputdir)
                    COMPREPLY=( $(compgen -d ) )
                    ;;
            esac
            ;;
        *)
            if [ "$prev" == "=" ]; then
                case "$pprev" in
                    --language)
                        COMPREPLY=( $(compgen -W "$langs" -- $cur))
                        ;;
                    --outputdir)
                        COMPREPLY=( $(compgen -d -- $cur) )
                        ;;
                esac
            else
                case "$prev" in
                    -l|--language)
                        COMPREPLY=( $(compgen -W "$langs" -- $cur))
                        ;;
                    -d|--outputdir)
                        COMPREPLY=( $(compgen -d -S "/" -- $cur) )
                        [[ $COMPREPLY == */ ]] && compopt -o nospace
                        ;;
                    *)
                        COMPREPLY=( $(compgen -f -X '!*.pds' -- $cur) $(compgen -d -S "/" -- $cur))
                        [[ $COMPREPLY == */ ]] && compopt -o nospace
                        ;;
                esac
            fi
            ;;
    esac
}

complete -F _Parsodus_completion Parsodus
