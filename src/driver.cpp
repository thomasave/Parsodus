/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Parsodus/driver.h"
#include "Parsodus/inputparser.h"
#include "Parsodus/config.h"

#include "g3log/g3log.hpp"
#include "g3log/logworker.hpp"

#include <fstream>
#include <iostream>
#include <sys/stat.h>

#if defined(_WIN32)

#include <io.h>
#define isatty(x) _isatty(x)
#define fileno(x) _fileno(x)
#define mkdir(x) _mkdir(x)
#include <direct.h>  
#define PATHSEP '/\\'

#else

#define mkdir(x) mkdir(x, 0755)
#include <unistd.h>
#define PATHSEP '/'

#endif

namespace {
    /**
     * Filter only valid identifier chars: alphanumeric, and not starting with a digit
     */
    std::string clean(std::string in) {
        std::string s;
        for (char c : in) {
            if ((s.length() && std::isalnum(c)) || std::isalpha(c) || c == '_')
                s += c;
        }
        return s;
    }


    std::string stripDirectory(std::string filename) {
        std::string dir = filename;

        std::size_t pos;
        // '/' can be used on most platforms (even windows)
        pos = dir.find_last_of(PATHSEP);
        if (pos != dir.npos) {
            dir = dir.substr(0,pos + 1);
        }

        if(dir == filename) {
            dir = "";
        }
        return dir;
    }

    /**
     * G3Log sink
     */
    class Sink {
        public:
            Sink(std::unique_ptr<std::ostream>&& infoOut) : m_infoOut(std::move(infoOut)) {}
            void ReceiveLogMessage(g3::LogMessageMover logEntry) {
                auto level = logEntry.get().level();

                std::string prefix = "";
                std::string suffix = "";

                if (isatty(fileno(stdin))) {
                    prefix = "\033[38;5;202;1m";
                    suffix = "\033[0m";
                }

                if (level == WARNING.text) {
                    std::cerr << prefix << logEntry.get().message() << suffix;
                } else if (level == INFO.text || level == DEBUG.text) {
                    if (m_infoOut)
                        *m_infoOut << logEntry.get().message();
                }
            }
        private:
            std::unique_ptr<std::ostream> m_infoOut;
    };
}


namespace pds {
    
    Driver::Driver(std::unique_ptr<BackendManager> backends, std::string inputfile, std::string outputdir, std::string language, std::string parsername, bool debug):
        m_backends(std::move(backends)), m_inputfile(inputfile), m_outputdir(outputdir), m_language(language), m_parsername(clean(parsername)), m_debug(debug) {
        }

    Driver::~Driver(){}

    int Driver::run() { 
        struct stat sb;
        if(stat(m_outputdir.c_str(), &sb) != 0) {
            int status; 
            status = mkdir(m_outputdir.c_str());
            if(status !=0 && errno != EEXIST){
                throw DriverException("The folder " + m_outputdir + " does not exist and we're unable to create it.");
            }
        }

        std::unique_ptr<g3::LogWorker> logworker{ g3::LogWorker::createLogWorker() };
        if (m_debug)
            logworker->addSink(std::make_unique<Sink>(std::make_unique<std::ofstream>(m_outputdir + "/debug.log")), &Sink::ReceiveLogMessage);
        else
            logworker->addSink(std::make_unique<Sink>(nullptr), &Sink::ReceiveLogMessage);
        g3::initializeLogging(logworker.get());

        if (!m_parsername.length()) throw DriverException("no valid parser name possible"); 

        
        /* Open parsodus config file, if possible */
        std::ifstream infile(m_inputfile);
        if (!infile.good()) {
            LOG(WARNING) << "Could not open file '" << m_inputfile << "' for reading" << std::endl;
            return 1;
        }
        
        
        Config config = InputParser::parseInput(infile, stripDirectory(m_inputfile));       
        Backend* back = m_backends->findBackend(m_language, config.parserType);
        if (!back) throw DriverException("Could not find a valid backend for language " + m_language + " and parser type: " + config.parserType);
		back->generateParser([this](std::string filename) -> std::unique_ptr<std::ostream> {
                return std::unique_ptr<std::ostream>(new std::ofstream(m_outputdir + "/" + filename));
				}, m_parsername, config);  

        return 0; 
    }

    DriverException::DriverException(std::string what): m_what(what) {} 
    const char* DriverException::what() const throw() {
        return m_what.c_str();
    }

}

