/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <iostream>
#include <fstream>
#include <vector>
#include "optparse.h"
#include "Parsodus/inputparser.h"
#include "Parsodus/backendmanager.h"
#include "Parsodus/backends/cppLR.h"
#include "Parsodus/driver.h"

int main(int argc, char** argv) {

    /* Set and parse command line arguments */
    optparse::OptionParser parser = optparse::OptionParser().description("Parsodus").usage("Parsodus [-d <outputdir>] [-l <language>] [-n <parsername>] [--debug] <inputfile.pds>");
    parser.add_help_option(true);
    parser.version("%prog 1.0");
    parser.add_option("-d", "--outputdir").dest("outputdir").help("Output the generated files to this directory\n[default: .]").metavar("<directory>").set_default(".");
    parser.add_option("-l", "--lang", "--language").dest("language").help("The programming language to generate source files for\n[default: c++]").metavar("<language>").set_default("c++");
    parser.add_option("-n", "--name").dest("parsername").help("Use this name for the generated parser, the default is based on the input file name").metavar("<parsername>");
    parser.add_option("--debug").dest("debug").action("store_true").set_default(false).help("Output debug logging");
    optparse::Values options = parser.parse_args(argc, argv);
    std::vector<std::string> args = parser.args();

    /* Check whether a parsodus config file is given  */
    if (args.size() != 1) {
        parser.print_usage(std::cerr);
        return 1;
    }

#ifdef _WIN32
    const char PATHSEP = '\\';
#else
    const char PATHSEP = '/';
#endif

    std::string parsername = options["parsername"];
  
    /* Set name of parser, if unset */
    if (parsername.empty()) {

        if (args[0].size() >= 4 && args[0].substr(args[0].size() - 4,4) == ".pds")
            parsername = args[0].substr(0, args[0].length() - 4);
        else
            parsername = args[0];

        std::size_t pos;
        // '/' can be used on most platforms (even windows)
        pos = parsername.find_last_of('/');
        if (pos != parsername.npos) {
            parsername = parsername.substr(pos + 1);
        }

        // strip platform specific as well
        pos = parsername.find_last_of(PATHSEP);
        if (pos != parsername.npos) {
            parsername = parsername.substr(pos + 1);
        }
    }

    auto backendManager = std::make_unique<pds::BackendManager>(pds::BackendManager());
    /* Register languages */
    backendManager->registerLR<pds::backends::CppLRBackend>();

    /* Create parserfile, if possible */
    try {
        pds::Driver driver(std::move(backendManager), args[0], options["outputdir"], options["language"], parsername, options.get("debug"));
        return driver.run();
    } catch (std::exception &err) {
        std::cout << err.what() << std::endl;
        return 1;
    }
}
