/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Parsodus/util/followset.h"
#include "Parsodus/util/symbols.h"
#include "Parsodus/grammar.h"

namespace pds {
namespace util {

FollowSet::FollowSet(const Grammar& g, const FirstSet& first)
{
    m_follow[EXTENDED_START].insert(EOF_PLACEHOLDER);

    bool changes = true;

    auto update = [&changes, this](std::string head, auto s) {
        s.erase("");
        for (std::string elem : s) {
            if (!m_follow[head].count(elem)) {
                changes = true;
                m_follow[head].insert(s.begin(), s.end());
                return;
            }
        }
    };

    while (changes) {
        changes = false;
        for (const auto& rule : g.rules) {
            const std::string& head = rule->head;
            auto it = rule->tail.begin(); //< Keep track of the start of the rest of the tail
            it++; //< The 'rest' of the tail
            for (std::size_t i = 0; i < rule->tail.size(); i++, it++) {
                if (g.variables.count(rule->tail[i])) {
                    std::set<std::string> restFirst = first(std::vector<std::string>(it, rule->tail.end()));
                    if (i == rule->tail.size() - 1 || restFirst.count("")) {
                        update(rule->tail[i], m_follow[head]);
                    }
                    if (i < rule->tail.size() - 1) {
                        update(rule->tail[i], restFirst);
                    }
                }
            }
        }
    }
}

std::set<std::string> FollowSet::operator()(std::string key) const {
    auto tmp = m_follow.find(key);
    if (tmp == m_follow.end())
        return {};
    return tmp->second;
}

} /* util */
} /* pds */
