/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Parsodus/lrtables/LALR1Itemset.h"

#include <cassert>

namespace pds {
namespace lr {

LALR1Itemset::LALR1Itemset() : LR1ItemsetBase<LALR1Itemset>()
{}

LALR1Itemset::LALR1Itemset(std::shared_ptr<Rule> start) : LR1ItemsetBase<LALR1Itemset>(start)
{}

bool LALR1Itemset::canMerge(const LALR1Itemset& rhs) const {
    if (rhs.m_items.size() != m_items.size()) {
        return false;
    }

    for (auto& rhsIt : rhs.m_items) {
        bool ok = false;
        for (auto& it : m_items) {
            if (it.rule == rhsIt.rule && it.dotIdx == rhsIt.dotIdx) {
                ok = true;
                break;
            }
        }
        if (!ok)
            return false;
    }

    return true;
}

bool LALR1Itemset::merge(const LALR1Itemset& rhs) {
    bool change = false;
    for (auto& toMerge : rhs.m_items) {
        bool found = false;
        for (auto& it : m_items) {
            if (it.rule == toMerge.rule && it.dotIdx == toMerge.dotIdx) {
                found = true;
                for (auto& la : toMerge.lookaheads) {
                    if (!it.lookaheads.count(la)) {
                        change = true;
                        it.lookaheads.insert(la);
                    }
                }
                break;
            }
        }
        assert(found);
    }
    return change;
}

} /* lr  */ 
} /* pds  */ 
