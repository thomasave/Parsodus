Parsodus(1) -- A language agnostic parser generator
============================================================

SYNOPSIS
--------

`Parsodus` [`-d` <outputdir>] [`-l` <language>] [`-n` <parsername>] [`--debug`] <inputfile.pds>


DESCRIPTION
-----------

Generate a parser from a Parsodus(5) configuration file

Options:

* `-h`, `--help`:
    show a help message and exit

* `--version`:
    show program's version number and exit

* `-d` <directory>, `--outputdir`=<directory>:
    Output the generated files to this directory  
    [default: .]

* `-l` <language>, `--lang`=<language>, `--language`=<language>:
    The programming language to generate source files for  
    [default: c++]

* `-n` <parsername>, `--name`=<parsername>:
    Use this name for the generated parser, the default is  
    based on the input file name

* `--debug`:
    Ouput debug logging, outputs to debug.log in the output directory


EXAMPLES
--------

`Parsodus -l c++ -d lexers -n MyParser parser.pds`

`Parsodus --language c++ --outputdir parsers --name MyParser parser.pds`

AUTHORS
-------

* Thomas Avé
* Robin Jadoul
* Kobe Wullaert

SEE ALSO
--------

Parsodus(5)
