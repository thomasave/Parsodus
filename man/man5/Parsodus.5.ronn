Parsodus(5) -- Syntax rules for Parsodus .pds files
=================================================

DESCRIPTION
-----------

Input files for Lexesis have a `.pds` extension and have a set of some very simple rules:
Variables in the grammar follow the regular expression `<[a-zA-Z_][a-zA-Z0-9_]*>`, and terminals use the same scheme, except using double quotes instead of angular brackets.

Furthermore, Parsodus uses a couple of key-value associations, including

- *parser*: the parsing algorithm to use
- *terminals*: a whitespace separated list of terminals
- *lexesis* (optional): a reference to a lexesis specification file. If given, terminals will be read from the lexesis file, and should as such not be specified separately in this file.
- *precedence* (optional): a whitespace separated list of `left`, `right`, or `nonassoc` followed by terminals, higher up is a higher precedence
- start: a variable to use as the start symbol
- grammar: a list of rules (see below)

A grammar rule is a variable followed by `::=` followed by a `|`-separated list of rule tails ended with a semicolon. A rule tail is a list of variables and terminals followed by an optional rule name of the form `[name]`.

    parser: lalr(1)
    terminals:
        "A"
    start: <s>
    grammar:
        <s> ::= "A" [single]
              | "A" "A" [double]
              ;

We are building an LALR(1) parser, with replacement rules, both starting from the start-symbol `<s>`, named appropriately `single` and `double`.

Conventionally, terminals are all caps, while variables are lowercase.

Comments are from a `#` to the end of the line.

AUTHORS
-------

* Thomas Avé
* Robin Jadoul
* Kobe Wullaert

SEE ALSO
--------

Parsodus(1)
