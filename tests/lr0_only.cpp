/*
 * Parsodus - A language agnostic parser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul, Kobe Wullaert
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Parsodus/lrtables/generator.h"
#include "Parsodus/lrtables/LR0Itemset.h"
#include "Parsodus/util/symbols.h"
#include "gtest/gtest.h"

#include <memory>

TEST(lr0, only) {
    using namespace pds;
    using namespace pds::lr;

	Grammar grammar;
	grammar.start = "s";
	grammar.variables = {"s","l",};
	grammar.terminals = {"X", "LBRACE", "RBRACE", "COMMA"};

    for (const std::pair<std::string, std::vector<std::string>>& p : std::vector<std::pair<std::string, std::vector<std::string>>>({
                {"s", {"LBRACE", "l", "RBRACE"}},
                {"s", {"X"}},
                {"l", {"s"}},
                {"l", {"l", "COMMA" ,"s"}},
            })) {

        grammar.rules.emplace_back(std::make_shared<Rule>(p.first, p.second));
    }

    {
        Generator<LR0Itemset> g(grammar);
        LRTable table;
        ASSERT_NO_THROW(table = g.generate());
        std::vector<std::map<std::string, std::pair<Action, std::size_t>>> act = {
            {
                {"LBRACE", {Action::SHIFT, 1}},
                {"X", {Action::SHIFT, 2}}
            }, {
                {"LBRACE", {Action::SHIFT, 1}},
                {"X", {Action::SHIFT, 2}},

            }, {
                {util::EOF_PLACEHOLDER, {Action::REDUCE, 1}},
                {"COMMA", {Action::REDUCE, 1}},
                {"LBRACE", {Action::REDUCE, 1}},
                {"RBRACE", {Action::REDUCE, 1}},
                {"X", {Action::REDUCE, 1}}
            }, {
                {util::EOF_PLACEHOLDER, {Action::ACCEPT, 0}}
            }, {
                {"COMMA", {Action::SHIFT, 6}},
                {"RBRACE", {Action::SHIFT, 7}}
            }, {
                {util::EOF_PLACEHOLDER, {Action::REDUCE, 2}},
                {"COMMA", {Action::REDUCE, 2}},
                {"LBRACE", {Action::REDUCE, 2}},
                {"RBRACE", {Action::REDUCE, 2}},
                {"X", {Action::REDUCE, 2}},
            }, {
                {"LBRACE", {Action::SHIFT, 1}},
                {"X", {Action::SHIFT, 2}}
            }, {
                {util::EOF_PLACEHOLDER, {Action::REDUCE, 0}},
                {"COMMA", {Action::REDUCE, 0}},
                {"LBRACE", {Action::REDUCE, 0}},
                {"RBRACE", {Action::REDUCE, 0}},
                {"X", {Action::REDUCE, 0}}
            }, {
                {util::EOF_PLACEHOLDER, {Action::REDUCE, 3}},
                {"COMMA", {Action::REDUCE, 3}},
                {"LBRACE", {Action::REDUCE, 3}},
                {"RBRACE", {Action::REDUCE, 3}},
                {"X", {Action::REDUCE, 3}}
            }};
        ASSERT_EQ(act.size(), table.act.size());
        for (std::size_t i = 0; i < act.size(); i++) {
            EXPECT_EQ(act[i], table.act[i]);
        }

        std::vector<std::map<std::string, size_t>> got = {
            { {"s", 3}},
            {{"l", 4}, {"s", 5}},
            {},
            {},
            {},
            {},
            {{"s", 8}},
            {},
            {}
        };
        ASSERT_EQ(got.size(), table.goto_.size());
        for (std::size_t i = 0; i < got.size(); i++) {
            EXPECT_EQ(got[i], table.goto_[i]);
        }
    }

}



